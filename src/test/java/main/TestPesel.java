package main;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestPesel {

    private Pesel pesel1, pesel2, pesel3, pesel4, pesel5, pesel6, pesel7, pesel8, pesel9;

    @BeforeEach
    public void init() throws Exception {
        pesel1 = new Pesel("96121407811");
        System.out.println(pesel1.birthDate());
        pesel2 = new Pesel("56810169716");
        System.out.println(pesel2.birthDate());
        pesel3 = new Pesel("56231589218");
        System.out.println(pesel3.birthDate());
        pesel4 = new Pesel("33523147916");
        System.out.println(pesel4.birthDate());
        pesel5 = new Pesel("22660618412");
        System.out.println(pesel5.birthDate());
        pesel6 = new Pesel("99123118714");
        System.out.println(pesel6.birthDate());
        pesel7 = new Pesel("00810164417");
        System.out.println(pesel7.birthDate());
        pesel8 = new Pesel("99723176718");
        System.out.println(pesel8.birthDate());
        pesel9 = new Pesel("11911104714");
        System.out.println(pesel8.birthDate());
    }

    @Test
    public void checkNull() {
        assertNotNull(pesel1, "pesel1 created?");
    }

    @Test
    public void checkBirthDate() {
        assertEquals(pesel1.birthDate(), "14.12.1996", "pesel1 birthdate == 14.12.1996?");
        assertEquals(pesel2.birthDate(), "01.01.1856", "pesel2 birthdate == 01.01.1856?");
        assertEquals(pesel3.birthDate(), "15.03.2056", "pesel3 birthdate == 15.03.2056?");
        assertEquals(pesel4.birthDate(), "31.12.2133", "pesel4 birthdate == 31.12.2133?");
        assertEquals(pesel5.birthDate(), "06.06.2222", "pesel5 birthdate == 06.06.2222?");
        assertEquals(pesel6.birthDate(), "31.12.1999", "pesel6 birthdate == 31.12.1999?");
        assertEquals(pesel7.birthDate(), "01.01.1800", "pesel7 birthdate == 01.01.1800?");
        assertEquals(pesel8.birthDate(), "31.12.2299", "pesel8 birthdate == 31.12.2299?");
        assertEquals(pesel9.birthDate(), "11.11.1811", "pesel9 birthdate == 11.11.1811?");
    }
}