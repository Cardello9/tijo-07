package main;

public class Pesel {
    private String pesel;

    Pesel(String pesel) throws Exception {
        if(pesel.length() == 11) {
            this.pesel = pesel;
        } else {
            throw new Exception();
        }
    }

    String birthDate() {
        char[] peselCharArr = this.pesel.toCharArray();
        char year1 = peselCharArr[0];
        char year2 = peselCharArr[1];
        char month1 = peselCharArr[2];
        char month2 = peselCharArr[3];
        char day1 = peselCharArr[4];
        char day2 = peselCharArr[5];

        String yearStr = year1 + "" + year2;
        int yearInt = Integer.parseInt(yearStr);
        String monthStr = month1 + "" + month2;
        Integer monthInt = Integer.parseInt(monthStr);
        String yearBeginning;

        if(monthInt >= 81 && monthInt <= 92) {
            monthInt -= 80;
            yearBeginning = "18";
        } else if (monthInt >= 21 && monthInt <= 32) {
            monthInt -= 20;
            yearBeginning = "20";
        } else if (monthInt>= 41 && monthInt <= 52) {
            monthInt -= 40;
            yearBeginning = "21";
        } else if (monthInt >= 61 && monthInt <= 72) {
            monthInt -= 60;
            yearBeginning = "22";
        } else {
            yearBeginning = "19";
        }

        if(monthInt < 10) {
            monthStr = "0" + monthInt.toString();
        } else {
            monthStr = monthInt.toString();
        }

        return day1 + "" + day2 + "." + monthStr + "." + yearBeginning + year1 + "" + year2;
    }
}
